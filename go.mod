module gitee.com/magicyu90/nuciotsdk

go 1.17

require (
	github.com/aws/aws-sdk-go v1.42.9
	github.com/eclipse/paho.mqtt.golang v1.3.5
	github.com/google/uuid v1.3.0
	github.com/jeanphorn/log4go v0.0.0-20190526082429-7dbb8deb9468
)

require github.com/toolkits/file v0.0.0-20160325033739-a5b3c5147e07 // indirect

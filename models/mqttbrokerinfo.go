package models

// MQTT Broker 信息
type MqttBrokerInfo struct {
	BrokerUrl    string // mqtt broker 信息
	Qos          byte   // qos
	Port         int    // 端口
	MqttUsername string // mqtt用户名
	MqttPass     string // mqtt密码
	IsCert       bool   // 是否开启证书
}

package models

// 设备运行信息
type DeviceData struct {
	IotBase
	DeviceId string `json:"deviceId"` // 设备编号
	//Content   string     `json:"content"`   // 运行信息内容
	DataItems []DataItem `json:"dataItems"` // 运行内容
}

// DataItem 数据项
type DataItem struct {
	DataKey   string `json:"dataKey"` // 数据key
	DataValue string `json:"dataVal"` // 数据内容
	Clock     int64  `json:"clock"`   // 时间戳
}

package models

// 设备状态
type DeviceStat struct {
	DeviceId string `json:"deviceId"`   // 设备编号
	Stat     int    `json:"deviceStat"` // 设备状态
	IotBase
}

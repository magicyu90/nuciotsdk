package models

// 物模型属性
type ThingProperty struct {
	DeviceId string      `json:"deviceId"` // 设备编号
	Params   interface{} `json:"params"`   // 运行参数
	Method   string      `json:"method"`   // 方法
	IotBase
}

// 物模型事件
type ThingEvent struct {
	DeviceId string      `json:"deviceId"` // 设备编号
	Params   interface{} `json:"params"`   // 运行参数
	Method   string      `json:"method"`   // 方法
	IotBase
}

// 物模型服务调用
type ThingServiceCall struct {
	DeviceId string      `json:"deviceId"` // 设备编号
	Params   interface{} `json:"params"`   // 参数
	Method   string      `json:"method"`   // 方法
	IotBase
}

// 物模型服务响应
type ThingServiceCallReply struct {
	DeviceId string      `json:"deviceId"` // 设备编号
	Params   interface{} `json:"params"`   // 回复参数
	Message  string      `json:"message"`  // 响应结果
	Method   string      `json:"method"`   // 方法
	IotBase2
}

// 物模型参数设置
type ThingPropertySet struct {
	DeviceId string      `json:"deviceId"` // 设备编号
	Params   interface{} `json:"params"`   // 设置参数
	Method   string      `json:"method"`   // 方法
	IotBase
}

// 物模型参数设置
type ThingPropertySetReply struct {
	DeviceId string      `json:"deviceId"` // 设备编号
	Params   interface{} `json:"params"`   // 设置参
	Message  string      `json:"message"`  // 响应结果
	Method   string      `json:"method"`   // 方法
	IotBase2
}

// 数据项
type ThingDataItem struct {
	DataKey   string      `json:"dataKey"` // 数据key
	DataValue interface{} `json:"dataVal"` // 数据内容
	Clock     int64       `json:"clock"`   // 时间戳
}

package models

// 设备存储信息
type DeviceStorage struct {
	S3Endpoint           string // s3 地址
	S3Bucket             string // s3 bucket
	S3AccessId           string // s3 access id
	S3AccessSecret       string // s3 秘钥
	LocalStoragePath     string // 本地存储路径
	LocalStorageUserName string // 本地存储用户名
	LocalStoragePass     string // 本地存储密码
}

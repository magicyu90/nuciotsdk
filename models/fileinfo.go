package models

// FileInfo 文件信息
type FileInfo struct {
	DeviceId string       `json:"deviceId"` // 设备编号
	FileName string       `json:"fileName"` // 文件名称
	FileType EnumFileType `json:"fileType"` // 文件类型
	FilePath string       `json:"filePath"` // 文件路径
	FileSize int          `json:"fileSize"` // 文件大小
	UserData string       `json:"userData"` // 用户扩展字段
	IotBase
}

// UploadReq 上传请求
type UploadReq struct {
	FileType  EnumFileType `json:"fileType"`  // 文件类型
	DeviceId  string       `json:"deviceId"`  // 设备编号
	StartTime int64        `json:"startTime"` // 开始时间
	EndTime   int64        `json:"endTime"`   // 结束时间
	IotBase
}

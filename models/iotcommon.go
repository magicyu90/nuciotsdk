package models

// 文件错误信息
type IotErrorInfo struct {
	Code    int    `json:"code"`    // 错误码
	Message string `json:"message"` // 错误消息
}

// 文件信息
type IotFileInfo struct {
	DeviceId string       `json:"deviceId"` // 设备编号
	FileName string       `json:"fileName"` // 文件名称
	Url      string       `json:"url"`      // 文件存储url
	FileSize int          `json:"fileSize"` // 文件大小
	FileType EnumFileType `json:"fileType"` // 文件类型
}

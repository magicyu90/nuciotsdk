package models

// Iot基类
type IotBase struct {
	TimeStamp int64  `json:"timestamp"` // 时间戳
	SessionId string `json:"sessionId"` // 会话Id
}

// Iot基类
type IotBase2 struct {
	Code int `json:"code"` // 平台返回code
	IotBase
}

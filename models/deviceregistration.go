package models

// 设备注册
type DeviceRegistrationReq struct {
	DeviceId        string `json:"deviceId"`        // 设备编号
	DeviceIp        string `json:"deviceIp"`        // 设备IP
	ProductKey      string `json:"productKey"`      // 产品标识
	DeviceType      string `json:"deviceType"`      // 设备类型
	ProtocolVersion string `json:"protocolVersion"` // 协议版本
	DeveloperId     string `json:"developerId"`     // 开发者Id
	ModelId         string `json:"modelId"`         // 模型Id
	GlobalId        string `json:"globalId"`        // 全局Id
	IotBase
	//RegItems        []RegItem `json:"regItems"`        // 关联设备注册项
}

// 子设备注册项
type RegItem struct {
	SubDeviceIp   string `json:"subDeviceIp"`   // 子设备IP
	SubDeviceType string `json:"subDeviceType"` // 子设备类型
	SubDeviceId   string `json:"subDeviceId"`   // 子设备编号
}

// 设备注册回复
type DeviceRegistrationReply struct {
	Message string `json:"message"` // 运行信息内容
	IotBase2
}

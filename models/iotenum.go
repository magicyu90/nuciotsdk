package models

// 文件类型
type EnumFileType int

const (
	Log   EnumFileType = iota // 日志 0
	Img                       // 图像 1
	Video                     // 视频 2
	Other                     // 其他 3
)

// 错误类型
type EnumErrorType int

const (
	SubscribeError    EnumErrorType = 1000 // 消息订阅错误
	RegistrationError EnumErrorType = 1001 // 注册错误
	AesError          EnumErrorType = 1002 // AES处理错误
	S3Error           EnumErrorType = 1003 // S3处理错误
	UnConnected       EnumErrorType = 1004 // 未连接错误
	SetupContentError EnumErrorType = 1005 // 组装消息错误
	DeserializeError  EnumErrorType = 1006 // 反序列化错误
	QueueError        EnumErrorType = 1007 // 队列处理错误
	LocalStorageError EnumErrorType = 1008 // 本地存储错误
	DataValidityError EnumErrorType = 1009 // 数据合法性错误
)

// 命令枚举
type EnumCommand int32

const (
	RegistrationCMD      EnumCommand = 0x00010000 // 设备注册 65536
	RegistrationReplyCMD EnumCommand = 0x00010001 // 设备注册响应 65537
	DeviceStatCMD        EnumCommand = 0x00010002 // 设备状态 65538
	DeviceStatReplyCMD   EnumCommand = 0x00010003 // 设备状态回复 65539
	DeviceDataCMD        EnumCommand = 0x00010004 // 设备运行信息 65540
	DeviceDataReplyCMD   EnumCommand = 0x00010005 // 设备运行信息回复 65541
	UploadCMD            EnumCommand = 0x00010006 // 上传请求 65542
	UploadReplyCMD       EnumCommand = 0x00010007 // 上传请求回复 65543
	FileInfoCMD          EnumCommand = 0x00010008 // 文件信息 65544
	FileInfoReplyCMD     EnumCommand = 0x00010009 // 文件信息响应 65545
	InternalHeartbeatCMD EnumCommand = 0x00000003 // 内部心跳 3
	OfflineCMD           EnumCommand = 0x00000002 // 下线遗嘱 2
	DeserializeErrorCMD  EnumCommand = 0x00000001 // 反序列化错误 1
	PropertyCMD          EnumCommand = 0x00030001 // 上报属性
	PropertyReplyCMD     EnumCommand = 0x00030002 // 属性回复
	EventCMD             EnumCommand = 0x00030003 // 事件上报
	EventReplyCMD        EnumCommand = 0x00030004 // 事件回复
	ServiceCallCMD       EnumCommand = 0x00030005 // 服务调用
	ServiceReplyCMD      EnumCommand = 0x00030006 // 服务应答
	PropertySetCMD       EnumCommand = 0x00030007 // 参数设置
	PropertySetReplyCMD  EnumCommand = 0x00030008 // 参数设置回复
)

// qos枚举类型
type EnumMqttQos int

const (
	Qos0 EnumMqttQos = 0 // Qos0
	Qos1 EnumMqttQos = 1 // Qos1
	Qos2 EnumMqttQos = 2 // Qos2
)

// 事件枚举类型
type EnumEvent string

const (
	ErrorEvent          EnumEvent = "ErrorEvent"     // 错误事件
	UploadRequestEvent  EnumEvent = "UploadRequest"  // 上传请求
	UploadCompleteEvent EnumEvent = "UploadComplete" // 上传完成
	ServiceCallEvent    EnumEvent = "ServiceCall"    // 服务调用
	PropertySetEvent    EnumEvent = "PropertySet"    // 参数设置
)

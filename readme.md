### Golang SDK

#### 说明
提供了注册设备、上传设备运行信息、上传设备状态的接，内部封装实现了获取注册设备资源占用情况的功能。并支持设备上传文件，接收平台下发指令功能等，便于开发者二次开发。

#### SDK 获取
- 最新版本：[1.1.1](https://ossnucloud.nuctech.com/nt-device/sdk/Golang/nuciotdemo.zip)

##### 环境要求
操作系统：win7,win10,Ubuntu,Centos

#### Golang SDK目录结构
-------------
```
|--cert mqtt连接所需证书
|--client 主程序
|--utils 工具帮助类,包括数据加密工具类、格式化工具类、时间转换工具类
|--models 项目依赖实体类,包括设备运行信息类、设备状态类、设备注册类、文件信息类、基础信息类
|--demo.go 示例程序，模拟开发者基于SDK进行开发。示例包括设备注册、设备发送状态、设备发送运行信息、设备上传文件以及对应回调函数的使用
```

#### SDK 功能列表
-------------
|    **模块功能**    | **功能点**                                                   |
| :----------------: | :----------------------------------------------------------- |
|      设备注册      | 注册设备相关平台至物联网平台                  |
|      自动重连      | 由于网络等原因，设备存在掉线的可能，SDK内置封装自动连接云平台 |
|      状态上报      | 向特定 topic 上报设备状态数据                          |
|      信息上报      | 向特定 topic 上报设备运行信息                          |
|      文件上传      | 上传文件至物联网S3平台            |
|      注册事件      | 为了满足业务开发者使用,可自行注册事件以便进行回调处理       |

#### SDK API 列表
-------------
|         函数         | 功能                                       |
| :------------------: | :----------------------------------------- |
|       NewIotClient     | 实例化SDK Client 相关选项                 |
|       Configure        | 配置初始化 |
|       SendRegistration | 设备注册                         |
|       SendDeviceData   | 上报设备运行信息                     |
|       SendDeviceStat   | 上报设备状态                        |
|       SendFileInfo     | 上传文件信息                               |
|       UploadFile2S3    | 上传文件到S3   |
|       RegisterEvent    | 注册事件     |

#### SDK 方法调用&数据类
-------------
##### NewIotClient

- 描述：新建IoT客户端实例，并初始化
- 输入参数

| 参数名称              | 参数数据类型          | 备注         |
| --------------------- | --------------------- | ------------ |
| DeviceReg | DeviceRegistrationReq | 注册信息 |
| MqttInfo        | MqttBrokerInfo        | mqtt信息 |
| DeviceStg         | DeviceStorage         | 存储信息 |

- 返回值

| 返回值数据类型 | 备注           |
| -------------- | -------------- |
| *nucIotClient   | IoT客户端实例 |

##### DeviceRegistrationReq

- 描述：设备注册请求

| 成员名称        | 成员数据类型 | 备注                                                         |
| --------------- | ------------ | ------------------------------------------------------------ |
| ProtocolVersion | string       | 协议版本<br />1.1.1                                          |
| DeveloperId     | string       | 开发者ID,由平台提供<br />               |
| ProductKey      | string       | 产品标识，如安检机、闸机、毫米波等等  |
| DeviceId        | string       | 设备唯一标识<br />如芯片号、MAC地址、设备序列号，需唯一确定 |
| DeviceType      | string       | 设备型号<br />      |
| DeviceIp        | string       | 设备IP地址       |

##### MqttBrokerInfo

- 描述：Mqtt信息

| 成员名称              | 成员数据类型          | 备注         |
| --------------------- | --------------------- | ------------ |
| BrokerUrl             | string                | broker地址 |
| MqttUsername          | string                | mqtt用户名 |
| MqttPass              | string                | mqtt密码   |
| Port                  | int                   | broker端口 |
| IsCert                | bool                  | 是否开启证书 |

##### DeviceStorage

- 描述：设备存储信息

| 成员名称              | 成员数据类型          | 备注         |
| --------------------- | --------------------- | ------------ |
| S3Bucket              | string                | S3Bucket|
| S3AccessId            | string                | S3AccessId |
| S3AccessSecret        | string                | S3秘钥   |
| LocalStoragePath      | string                | 共享目录url         |
| LocalStorageUserName  | string                | 共享目录用户名       |
| LocalStoragePass      | string                | 共享目录密码         |

##### SendRegistration

- 描述：注册设备
- 返回值

| 返回值数据类型 | 备注                                                         |
| -------------- | ------------------------------------------------------------ |
| int            | 返回码<br />0：成功<br />其他：失败（具体失败原因参考[错误码](#错误码)列表） |

##### SendDeviceStat

- 描述：上传设备状态
- 输入参数

| 参数名称 | 参数数据类型 | 备注                                                         |
| -------- | ------------ | ------------------------------------------------------------ |
| deviceId | string       | 所注册设备的唯一标识                                         |
| stat     | int          | 所注册设备的状态<br />0：初始化<br />1：正常<br />2：故障<br />3：诊断<br />4：关机<br /> |

##### UploadFile2S3

- 描述：上传文件到S3
- 输入参数

| 参数名称 | 参数数据类型 | 备注                                                         |
| ----------- | ------------ | ------------------------------------------------------------ |
| deviceId    | string       | 设备标识                                                     |
| filePath    | string       | 文件本地路径                                                 |
| contentType | string       | 数据类型，可遵循 Web 数据格式，如【image/png】，【text/plain】等 |
| fileType    | EnumFileType | 文件类型 具体类型参考[文件类型](#文件类型)列表|


##### SendFileInfo

- 描述：上传成功后上报文件信息，可通过订阅上传成功回调获取上传成功的文件属性
- 输入参数

| 参数名称 | 参数数据类型 | 备注                                                         |
| -------- | ------------ | ------------------------------------------------------------ |
| deviceId | string       | 所注册设备的唯一标识                                         |
| fileName | string       | 文件名称<br />可通过 [OnUploadComplete](#OnUploadComplete) 上传文件接口的回调方法中的 [IotFileInfo](#IotFileInfo) 获取，对应成员为 `FileName` |
| fileType | EnumFileType | 文件类型<br />可通过 [OnUploadComplete](#OnUploadComplete) 上传文件接口的回调方法中的 [IotFileInfo](#IotFileInfo) 获取，对应成员为 `FileType` |
| filePath | string       | 文件路径<br />可通过 [OnUploadComplete](#OnUploadComplete) 上传文件接口的回调方法中的 [IotFileInfo](#IotFileInfo) 获取，对应成员为 `Url` |
| fileSize | int          | 文件大小<br />可通过 [OnUploadComplete](#OnUploadComplete) 上传文件接口的回调方法中的 [IotFileInfo](#IotFileInfo) 获取，对应成员为 `FileSize` |

------------------------------
#### 方法事件

##### ErrorEvent

- 描述：IoT 客户端调用任意接口发生错误事件
- 回调参数

| 参数名称 | 参数数据类型 | 备注     |
| --------- | ------------ | -------- |
| arg1 | IotErrorInfo | 错误信息 |

##### IotErrorInfo

- 描述：错误信息

| 成员名称 | 成员数据类型 | 备注                                                         |
| -------- | ------------ | ------------------------------------------------------------ |
| Code     | int          | 错误码，具体参考[错误码](#错误码)列表                        |
| Msg      | string       | 错误内容<br />包含封装所给出的错误相关数据信息、文件信息，找到对应出错的标识性内容。还可能包含错误发生时，运行时抛出的异常信息。 |

##### UploadCompleteEvent

- 描述：IoT 客户端调用上传文件接口上传完成事件
- 回调参数

| 参数名称 | 参数数据类型 | 备注                                              |
| -------- | ------------ | ------------------------------------------------- |
| arg1     | bool         | 文件上传是否成功<br />false：失败<br />true：成功 |
| arg2     | IotFileInfo  | 文件信息                                          |


##### IotFileInfo

- 描述：文件信息，上传成功时应有对应的文件信息

| 成员名称 | 成员数据类型 | 备注               |
| ------------ | ------------ | ------------------ |
| DeviceId     | string       | 设备标识           |
| FileName     | string       | 文件名称           |
| FileSize     | int          | 文件大小           |
| FileType     | EnumFileType | 文件类型           |
| Url          | string       | 文件在 S3 上的路径 |
| UserData     | string       | 用户自定义内容     |

##### UploadRequestEvent

- 描述：IoT 客户端收到文件上传请求事件
- 回调参数

| 参数名称 | 参数数据类型 | 备注                                              |
| -------- | ------------ | ---------------------------------------------- |
| uploadReq | UploadReq  | 文件上传请求                                      |

##### UploadReq

- 描述：文件信息，上传成功时应有对应的文件信息

| 成员名称 | 成员数据类型 | 备注                |
| ------------ | ------------ | -----------------|
| StartTime    | long         | 开始时间          |
| EndTime      | long         | 结束时间          |
| FileType     | EnumFileType | 文件类型          |            

#### 错误码

封装库内所包含的数据结构 `EnumErrorType` 所对应的内容，用于枚举错误码。

| 错误码 |      错误描述      |     枚举名称      |
| :----: | :----------------: | :---------------: |
|  1000  |    消息订阅错误    |  SubscribeError   |
|  1001  |      注册错误      | RegistrationError |
|  1002  |    消息加密错误    |     AesError      |
|  1003  |     S3处理错误     |      S3Error      |
|  1004  | 与云端建立通讯错误 |    UnConnected    |
|  1005  |    消息组装错误    | SetupContentError |
|  1006  |  消息反序列化错误  | DeserializeError   |
|  1007  |  消息队列处理错误  |    QueueError      |
|  1008  |  本地存储错误    |    LocalStorageError|
|  1009  |  数据合法性错误  |   DataValidityError |

#### 文件类型

封装库内所包含的数据结构 `EnumFileType` 所对应的内容，用于枚举错误码。

| 文件类型 |      枚举类型      |
| :----: | :----------------: |
|  日志  |      Log    |
|  图像  |      Img      |
|  视频  |    Video    |   
|  其他  |    Other    |   


#### 代码示例
```go

package main

import (
	"fmt"
	"time"

	"gitee.com/magicyu90/nuciotsdk/client"
	"gitee.com/magicyu90/nuciotsdk/models"
	"gitee.com/magicyu90/nuciotsdk/utils"
)

// 错误回调
func OnError(args ...interface{}) {
	e := args[0].(*models.IotErrorInfo)
	fmt.Printf("OnError,code:%d,message:%s\n", e.Code, e.Message)
}

// 完成上传回调
func OnUploadComplete(args ...interface{}) {
	fmt.Println("OnUploadComplete...")
	// 发送文件信息
	ok := args[0].(bool)
	iotFileInfo := args[1].(*models.IotFileInfo)
	if ok {
		fmt.Printf("文件:%s上传1成功,上传文件信息...\n", iotFileInfo.FileName)
		n.SendFileInfo(iotFileInfo.DeviceId, iotFileInfo.FileName, iotFileInfo.Url, iotFileInfo.FileSize, iotFileInfo.FileType, iotFileInfo.UserData)
	} else {
		fmt.Printf("文件:%s上传失败...\n", iotFileInfo.FileName)
	}

}

var deviceId = "xxxx"

// 设备信息
var deviceInfo = models.DeviceRegistrationReq{
	DeveloperId:     "xxx", //开发者ID
	ProtocolVersion: "1.1.1",
	DeviceId:        deviceId, //设备编号
	DeviceType:      "xxx", //设备型号
	ProductKey:      "xxx",    //产品类型
	DeviceIp:        "192.168.1.1",
}

// mqtt信息
var mqttInfo = models.MqttBrokerInfo{
	BrokerUrl: "********", //mqtt地址
	Port:         10884,           //mqtt端口
	MqttUsername: "****",            //mqtt用户名
	MqttPass:     "****", //mqtt密码
	IsCert:       false,           //是否开启证书
}

// 存储信息
var storageInfo = models.DeviceStorage{
	S3Bucket:             "xxxx",                                   //s3 bucket
	S3AccessId:           "******",                     //s3 accessId
	S3AccessSecret:       "********", //s3 accesstoken
	LocalStoragePath:     "xxxx",              //共享目录
	LocalStorageUserName: "xxx",                                     //共享目录用户名
	LocalStoragePass:     "xxxx",                               //共享目录密码
}

// 初始化客户端
var n = client.NewIotClient(deviceInfo, storageInfo, mqttInfo)

// 主函数
func main() {
	// 初始化配置
	n.Configure()
	// 注册错误事件
	n.RegisterEvent(models.ErrorEvent, OnError)
	// 注册上传完成事件
	n.RegisterEvent(models.UploadCompleteEvent, OnUploadComplete)

	retryTimes := 1
	// 设备注册
	for {
		fmt.Printf("开始进行第%d次注册...\n", retryTimes)
		result := n.SendRegistration()
		fmt.Printf("注册结果:%d\n", result)
		if result == 0 {
			break
		}
		retryTimes += 1
		time.Sleep(time.Duration(2) * time.Second)
	}
	fmt.Println("上报设备状态")
	go func() {
		for {
			// 发送设备状态
			n.SendDeviceStat(deviceId, 1)
			time.Sleep(time.Duration(2) * time.Second)
		}
	}()

	fmt.Println("上报设备运行信息")
	go func() {
		for {
			// 发送设备状态
			n.SendDeviceStat(deviceId, 1)
			dataItems := [2]models.DataItem{
				{DataKey: "k1", DataValue: "10", Clock: utils.GetCurrentTimestamp()},
				{DataKey: "k2", DataValue: "20", Clock: utils.GetCurrentTimestamp()}}
			n.SendDeviceData(deviceId, dataItems[:])
			time.Sleep(time.Duration(2) * time.Second)
		}
	}()

	fmt.Println("上传图片至s3")
	n.UploadFile2S3(deviceId, "C:/xxxx/lake.jpg", models.Img, "")

	select {} // 阻塞
}
```
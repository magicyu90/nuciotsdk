package utils

import (
	"bytes"
	"encoding/binary"
	"net/http"
	"os"
	"time"
	"unsafe"
)

// @title    BytesCombine
// @description   字节合并
// @return    []byte   结果
func BytesCombine(pBytes ...[]byte) []byte {
	var buffer bytes.Buffer
	for index := 0; index < len(pBytes); index++ {
		buffer.Write(pBytes[index])
	}
	return buffer.Bytes()
}

// @title    GetCurrentTimestamp
// @description   获取当前时间时间戳
// @return    int64   时间戳
func GetCurrentTimestamp() int64 {
	return time.Now().UnixNano() / 1e6 //时间戳(毫秒)
}

// @title    Int2Bytes
// @description   整数转换成字节
// @param     n   int   整数值
// @return    byte[]   字节数组
func Int2Bytes(n int) []byte {
	x := int32(n)
	bytesBuffer := bytes.NewBuffer([]byte{})
	// if IsLittleEndian() {
	// 	binary.Write(bytesBuffer, binary.LittleEndian, x)
	// } else {
	// 	binary.Write(bytesBuffer, binary.BigEndian, x)
	// }
	// 大端字节序
	binary.Write(bytesBuffer, binary.BigEndian, x)
	return bytesBuffer.Bytes()
}

// @title    Bytes2Int
// @description   字节转换成整数
// @param     bys   []byte   字节内容
// @return    int   整形值
func Bytes2Int(bys []byte) int {
	bytebuff := bytes.NewBuffer(bys)
	var data int
	binary.Read(bytebuff, binary.BigEndian, &data)
	return int(data)
}

// @title    IsLittleEndian
// @description   大小端判断
// @return    bool   结果
func IsLittleEndian() bool {
	var value int32 = 1 // 占4byte 转换成16进制 0x00 00 00 01
	// 大端(16进制)：00 00 00 01
	// 小端(16进制)：01 00 00 00
	pointer := unsafe.Pointer(&value)
	pb := (*byte)(pointer)
	return *pb == 1
}

// @title    GetFileContentType
// @description   获取文件扩展名
// @return   string   结果
// @return   error   错误
func GetFileContentType(out *os.File) (string, error) {
	// Only the first 512 bytes are used to sniff the content type.
	buffer := make([]byte, 512)
	_, err := out.Read(buffer)
	if err != nil {
		return "", err
	}
	// Use the net/http package's handy DectectContentType function. Always returns a valid
	// content-type by returning "application/octet-stream" if no others seemed to match.
	contentType := http.DetectContentType(buffer)
	return contentType, nil
}

// @title    FileExist
// @description   验证文件或文件夹是否存在
// @param     path   string   文件路径
// @return   bool   是否存在
func FileExist(path string) bool {
	_, err := os.Lstat(path)
	return !os.IsNotExist(err)
}

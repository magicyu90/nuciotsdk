package utils

import (
	"bytes"
	"crypto/aes"
	"crypto/cipher"
)

// 定义常量
const (
	// 秘钥
	PUBLICKEY = "XWCtbPwDn4e6glqc"
	// 偏移量
	IV = "0000000000000000"
)

// @title    PKCS5Padding
// @description   填充明文
func PKCS5Padding(plaintext []byte, blockSize int) []byte {
	padding := blockSize - len(plaintext)%blockSize
	padtext := bytes.Repeat([]byte{byte(padding)}, padding)
	return append(plaintext, padtext...)
}

// @title    PKCS5UnPadding
// @description   去除填充数据
func PKCS5UnPadding(origData []byte) []byte {
	length := len(origData)
	unpadding := int(origData[length-1])
	return origData[:(length - unpadding)]
}

// @title    PKCS7Padding
// @description   填充明文
func PKCS7Padding(ciphertext []byte, blockSize int) []byte {
	padding := blockSize - len(ciphertext)%blockSize
	padtext := bytes.Repeat([]byte{byte(padding)}, padding)
	return append(ciphertext, padtext...)
}

// @title    PKCS7UnPadding
// @description   去除填充数据
func PKCS7UnPadding(origData []byte) []byte {
	length := len(origData)
	unpadding := int(origData[length-1])
	return origData[:(length - unpadding)]
}

// @title    AesEncrypt
// @description   AES加密
func AesEncrypt(origData []byte) ([]byte, error) {
	block, err := aes.NewCipher([]byte(PUBLICKEY))
	if err != nil {
		return nil, err
	}

	//AES分组长度为 128 位，所以 blockSize=16 字节
	blockSize := block.BlockSize()
	origData = PKCS5Padding(origData, blockSize)
	//初始向量的长度必须等于块block的长度16字节
	blockMode := cipher.NewCBCEncrypter(block, []byte(IV))
	crypted := make([]byte, len(origData))
	blockMode.CryptBlocks(crypted, origData)
	return crypted, nil
}

// @title    AesDecrypt
// @description   AES解密
func AesDecrypt(crypted []byte) ([]byte, error) {
	block, err := aes.NewCipher([]byte(PUBLICKEY))
	if err != nil {
		return nil, err
	}
	blockMode := cipher.NewCBCDecrypter(block, []byte(IV))
	origData := make([]byte, len(crypted))
	blockMode.CryptBlocks(origData, crypted)
	origData = PKCS5UnPadding(origData)
	return origData, nil
}
